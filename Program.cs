﻿using System;

// Interface for the base component (Fruit)
public interface IFruit
{
    string GetName();
}

// Concrete implementation of the base component (Fruit)
public class SimpleFruit : IFruit
{
    private string _name;

    public SimpleFruit(string name)
    {
        _name = name;
    }

    public string GetName()
    {
        return _name;
    }
}

// Decorator base class
public abstract class FruitDecorator : IFruit
{
    protected IFruit _fruit;

    public FruitDecorator(IFruit fruit)
    {
        _fruit = fruit;
    }

    public virtual string GetName()
    {
        return _fruit.GetName();
    }
}

// Concrete decorator for adding color
public class ColorDecorator : FruitDecorator
{
    private string _color;

    public ColorDecorator(IFruit fruit, string color) : base(fruit)
    {
        _color = color;
    }

    public override string GetName()
    {
        return $"{base.GetName()} ({_color} color)";
    }
}

// Concrete decorator for adding taste
public class TasteDecorator : FruitDecorator
{
    private string _taste;

    public TasteDecorator(IFruit fruit, string taste) : base(fruit)
    {
        _taste = taste;
    }

    public override string GetName()
    {
        return $"{base.GetName()} with {_taste} taste";
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Create a simple fruit
        IFruit apple = new SimpleFruit("Apple");

        // Add color to the fruit
        apple = new ColorDecorator(apple, "Red");

        // Add taste to the fruit
        apple = new TasteDecorator(apple, "Sweet");

        Console.WriteLine(apple.GetName()); // Output: Apple (Red color) with Sweet taste
    }
}
